///////////////////////
// Node settings
///////////////////////
// Enable debug prints to serial monitor
//#define MY_DEBUG
// Enable and select radio type attached
#define MY_RADIO_NRF24
// Enable repeater functionality for this node
#define MY_REPEATER_FEATURE

#include <SPI.h>
#include <MySensors.h>  
#include <DallasTemperature.h>
#include <OneWire.h>

///////////////////////
// Sensors
///////////////////////
#define TEMP     3
#define RELAY_1  4
#define RELAY_2  5
#define RELAY_3  6

///////////////////////
// Temperature settings
///////////////////////
// Send temperature only if changed? 1 = Yes 0 = No
#define COMPARE_TEMP 1
OneWire oneWire(TEMP); // Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
DallasTemperature sensors(&oneWire); // Pass the oneWire reference to Dallas Temperature. 
float lastTemperature;
bool receivedConfig = false;
bool metric = true;
unsigned long SLEEP_TIME = 30000; // Sleep time between reads (in milliseconds)
// Initialize temperature message
MyMessage msg(0,V_TEMP);

///////////////////////
// Relay settings
///////////////////////
// GPIO value to write to turn on/off attached relay
#define RELAY_ON  1
#define RELAY_OFF 0

void before()
{
  // Startup up the OneWire library
  sensors.begin();

  // Setup relays
  pinMode(RELAY_1, OUTPUT);
  digitalWrite(RELAY_1, loadState(RELAY_1)?RELAY_ON:RELAY_OFF);
  pinMode(RELAY_2, OUTPUT);
  digitalWrite(RELAY_2, loadState(RELAY_1)?RELAY_ON:RELAY_OFF);
  pinMode(RELAY_3, OUTPUT);
  digitalWrite(RELAY_3, loadState(RELAY_1)?RELAY_ON:RELAY_OFF);
}

void setup()
{
  // requestTemperatures() will not block current thread
  sensors.setWaitForConversion(false);
}

void presentation()
{
  // Send the sketch version information to the gateway and Controller
  sendSketchInfo("Aquarium", "1.0");
  present(TEMP, S_TEMP);
  present(RELAY_1, S_BINARY);
  present(RELAY_2, S_BINARY);
  present(RELAY_3, S_BINARY);
}


void loop()
{
// Fetch temperatures from Dallas sensors
  sensors.requestTemperatures();

  // query conversion time and sleep until conversion completed
  int16_t conversionTime = sensors.millisToWaitForConversion(sensors.getResolution());
  // sleep() call can be replaced by wait() call if node need to process incoming messages (or if node is repeater)
  wait(conversionTime);

  // Read temperatures and send them to controller

  // Fetch and round temperature to one decimal
  float temperature = static_cast<float>(static_cast<int>((getControllerConfig().isMetric?sensors.getTempCByIndex(0):sensors.getTempFByIndex(0  )) * 10.)) / 10.;

  Serial.print("Temperature: ");
  Serial.println(temperature);

  // Only send data if temperature has changed and no error
  #if COMPARE_TEMP == 1
  if (lastTemperature != temperature && temperature != -127.00 && temperature != 85.00) {
  #else
  if (temperature != -127.00 && temperature != 85.00) {
  #endif
    // Send in the new temperature
    send(msg.setSensor(TEMP).set(temperature,1));
    // Save new temperatures for next compare
    lastTemperature=temperature;
  }
  wait(SLEEP_TIME);
}

void receive(const MyMessage &message)
{
  // We only expect one type of message from controller. But we better check anyway.
  if (message.type==V_STATUS) {
    // Change relay state
    digitalWrite(message.sensor, message.getBool()?RELAY_ON:RELAY_OFF);
    // Store state in eeprom
    saveState(message.sensor, message.getBool());
    // Write some debug info
    Serial.print("Incoming change for sensor:");
    Serial.print(message.sensor);
    Serial.print(", New status: ");
    Serial.println(message.getBool());
  }
}
